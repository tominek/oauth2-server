<?php

namespace Tominek\OAuth2\Server\Tests\Grant;

use Symfony\Component\HttpFoundation\Request;
use Tominek\OAuth2\Server\Tests\Stubs\AccessTokenEntity;
use Tominek\OAuth2\Server\Tests\Stubs\ClientEntity;
use Tominek\OAuth2\Server\Tests\Stubs\CryptTraitStub;
use Tominek\OAuth2\Server\Tests\Stubs\ScopeEntity;
use Tominek\OAuth2\Server\Tests\Stubs\StubResponseType;
use Tominek\OAuth2\Server\Tests\Stubs\UserEntity;
use PHPUnit\Framework\TestCase;
use Tominek\OAuth2\Server\CryptKey;
use Tominek\OAuth2\Server\Exception\OAuthServerException;
use Tominek\OAuth2\Server\Exception\UniqueTokenIdentifierConstraintViolationException;
use Tominek\OAuth2\Server\Grant\ImplicitGrant;
use Tominek\OAuth2\Server\Repositories\AccessTokenRepositoryInterface;
use Tominek\OAuth2\Server\Repositories\ClientRepositoryInterface;
use Tominek\OAuth2\Server\Repositories\RefreshTokenRepositoryInterface;
use Tominek\OAuth2\Server\Repositories\ScopeRepositoryInterface;
use Tominek\OAuth2\Server\RequestTypes\AuthorizationRequest;
use Tominek\OAuth2\Server\ResponseTypes\RedirectResponse;

class ImplicitGrantTest extends TestCase
{
    const DEFAULT_SCOPE = 'basic';

    /**
     * CryptTrait stub
     */
    protected $cryptStub;

    public function setUp()
    {
        $this->cryptStub = new CryptTraitStub();
    }

    public function testGetIdentifier()
    {
        $grant = new ImplicitGrant(new \DateInterval('PT10M'));
        $this->assertEquals('implicit', $grant->getIdentifier());
    }

    public function testCanRespondToAccessTokenRequest()
    {
        $grant = new ImplicitGrant(new \DateInterval('PT10M'));

        $this->assertFalse(
            $grant->canRespondToAccessTokenRequest(new Request())
        );
    }

    /**
     * @expectedException \LogicException
     */
    public function testRespondToAccessTokenRequest()
    {
        $grant = new ImplicitGrant(new \DateInterval('PT10M'));
        $grant->respondToAccessTokenRequest(
            new Request(),
            new StubResponseType(),
            new \DateInterval('PT10M')
        );
    }

    public function testCanRespondToAuthorizationRequest()
    {
        $grant = new ImplicitGrant(new \DateInterval('PT10M'));

        $request = new Request([
            'response_type' => 'token',
            'client_id' => 'foo',
        ]);

        $this->assertTrue($grant->canRespondToAuthorizationRequest($request));
    }

    /**
     * @throws OAuthServerException
     */
    public function testValidateAuthorizationRequest()
    {
        $client = new ClientEntity();
        $client->setRedirectUri('http://foo/bar');
        $clientRepositoryMock = $this->getMockBuilder(ClientRepositoryInterface::class)->getMock();
        $clientRepositoryMock->method('getClientEntity')->willReturn($client);

        $scopeRepositoryMock = $this->getMockBuilder(ScopeRepositoryInterface::class)->getMock();
        $scopeEntity = new ScopeEntity();
        $scopeRepositoryMock->method('getScopeEntityByIdentifier')->willReturn($scopeEntity);
        $scopeRepositoryMock->method('finalizeScopes')->willReturnArgument(0);

        $grant = new ImplicitGrant(new \DateInterval('PT10M'));
        $grant->setClientRepository($clientRepositoryMock);
        $grant->setScopeRepository($scopeRepositoryMock);
        $grant->setDefaultScope(self::DEFAULT_SCOPE);

        $request = new Request([
            'response_type' => 'code',
            'client_id' => 'foo',
            'redirect_uri' => 'http://foo/bar',
        ]);

        $this->assertTrue($grant->validateAuthorizationRequest($request) instanceof AuthorizationRequest);
    }

    /**
     * @throws OAuthServerException
     */
    public function testValidateAuthorizationRequestRedirectUriArray()
    {
        $client = new ClientEntity();
        $client->setRedirectUri(['http://foo/bar']);
        $clientRepositoryMock = $this->getMockBuilder(ClientRepositoryInterface::class)->getMock();
        $clientRepositoryMock->method('getClientEntity')->willReturn($client);

        $scopeRepositoryMock = $this->getMockBuilder(ScopeRepositoryInterface::class)->getMock();
        $scopeEntity = new ScopeEntity();
        $scopeRepositoryMock->method('getScopeEntityByIdentifier')->willReturn($scopeEntity);
        $scopeRepositoryMock->method('finalizeScopes')->willReturnArgument(0);

        $grant = new ImplicitGrant(new \DateInterval('PT10M'));
        $grant->setClientRepository($clientRepositoryMock);
        $grant->setScopeRepository($scopeRepositoryMock);
        $grant->setDefaultScope(self::DEFAULT_SCOPE);

        $request = new Request([
            'response_type' => 'code',
            'client_id' => 'foo',
            'redirect_uri' => 'http://foo/bar',
        ]);

        $this->assertTrue($grant->validateAuthorizationRequest($request) instanceof AuthorizationRequest);
    }

    /**
     * @throws OAuthServerException
     */
    public function testValidateAuthorizationRequestMissingClientId()
    {
        $clientRepositoryMock = $this->getMockBuilder(ClientRepositoryInterface::class)->getMock();

        $grant = new ImplicitGrant(new \DateInterval('PT10M'));
        $grant->setClientRepository($clientRepositoryMock);

        $request = new Request([
            'response_type' => 'code',
        ]);

        $this->expectException(OAuthServerException::class);
        $this->expectExceptionCode(3);
        $grant->validateAuthorizationRequest($request);
    }

    /**
     * @throws OAuthServerException
     */
    public function testValidateAuthorizationRequestInvalidClientId()
    {
        $clientRepositoryMock = $this->getMockBuilder(ClientRepositoryInterface::class)->getMock();
        $clientRepositoryMock->method('getClientEntity')->willReturn(null);

        $grant = new ImplicitGrant(new \DateInterval('PT10M'));
        $grant->setClientRepository($clientRepositoryMock);

        $request = new Request([
            'response_type' => 'code',
            'client_id' => 'foo',
        ]);

        $this->expectException(OAuthServerException::class);
        $this->expectExceptionCode(4);
        $grant->validateAuthorizationRequest($request);
    }

    /**
     * @throws OAuthServerException
     */
    public function testValidateAuthorizationRequestBadRedirectUriString()
    {
        $client = new ClientEntity();
        $client->setRedirectUri('http://foo/bar');
        $clientRepositoryMock = $this->getMockBuilder(ClientRepositoryInterface::class)->getMock();
        $clientRepositoryMock->method('getClientEntity')->willReturn($client);

        $grant = new ImplicitGrant(new \DateInterval('PT10M'));
        $grant->setClientRepository($clientRepositoryMock);

        $request = new Request([
            'response_type' => 'code',
            'client_id' => 'foo',
            'redirect_uri' => 'http://bar',
        ]);

        $this->expectException(OAuthServerException::class);
        $this->expectExceptionCode(4);
        $grant->validateAuthorizationRequest($request);
    }

    /**
     * @throws OAuthServerException
     */
    public function testValidateAuthorizationRequestBadRedirectUriArray()
    {
        $client = new ClientEntity();
        $client->setRedirectUri(['http://foo/bar']);
        $clientRepositoryMock = $this->getMockBuilder(ClientRepositoryInterface::class)->getMock();
        $clientRepositoryMock->method('getClientEntity')->willReturn($client);

        $grant = new ImplicitGrant(new \DateInterval('PT10M'));
        $grant->setClientRepository($clientRepositoryMock);

        $request = new Request([
            'response_type' => 'code',
            'client_id' => 'foo',
            'redirect_uri' => 'http://bar',
        ]);

        $this->expectException(OAuthServerException::class);
        $this->expectExceptionCode(4);
        $grant->validateAuthorizationRequest($request);
    }

    /**
     * @throws OAuthServerException
     */
    public function testCompleteAuthorizationRequest()
    {
        $authRequest = new AuthorizationRequest();
        $authRequest->setAuthorizationApproved(true);
        $authRequest->setClient(new ClientEntity());
        $authRequest->setGrantTypeId('authorization_code');
        $authRequest->setUser(new UserEntity());

        $accessTokenRepositoryMock = $this->getMockBuilder(AccessTokenRepositoryInterface::class)->getMock();
        $accessTokenRepositoryMock->method('getNewToken')->willReturn(new AccessTokenEntity());
        $accessTokenRepositoryMock->method('persistNewAccessToken')->willReturnSelf();

        $grant = new ImplicitGrant(new \DateInterval('PT10M'));
        $grant->setPrivateKey(new CryptKey('file://' . __DIR__ . '/../Stubs/private.key'));
        $grant->setAccessTokenRepository($accessTokenRepositoryMock);

        $this->assertTrue($grant->completeAuthorizationRequest($authRequest) instanceof RedirectResponse);
    }

    /**
     * @throws OAuthServerException
     */
    public function testCompleteAuthorizationRequestDenied()
    {
        $authRequest = new AuthorizationRequest();
        $authRequest->setAuthorizationApproved(false);
        $authRequest->setClient(new ClientEntity());
        $authRequest->setGrantTypeId('authorization_code');
        $authRequest->setUser(new UserEntity());

        $accessTokenRepositoryMock = $this->getMockBuilder(AccessTokenRepositoryInterface::class)->getMock();
        $accessTokenRepositoryMock->method('getNewToken')->willReturn(new AccessTokenEntity());
        $accessTokenRepositoryMock->method('persistNewAccessToken')->willReturnSelf();

        $grant = new ImplicitGrant(new \DateInterval('PT10M'));
        $grant->setPrivateKey(new CryptKey('file://' . __DIR__ . '/../Stubs/private.key'));
        $grant->setAccessTokenRepository($accessTokenRepositoryMock);

        $this->expectException(OAuthServerException::class);
        $this->expectExceptionCode(9);
        $grant->completeAuthorizationRequest($authRequest);
    }

    /**
     * @throws OAuthServerException
     */
    public function testAccessTokenRepositoryUniqueConstraintCheck()
    {
        $authRequest = new AuthorizationRequest();
        $authRequest->setAuthorizationApproved(true);
        $authRequest->setClient(new ClientEntity());
        $authRequest->setGrantTypeId('authorization_code');
        $authRequest->setUser(new UserEntity());

        /** @var AccessTokenRepositoryInterface|\PHPUnit_Framework_MockObject_MockObject $accessTokenRepositoryMock */
        $accessTokenRepositoryMock = $this->getMockBuilder(AccessTokenRepositoryInterface::class)->getMock();
        $accessTokenRepositoryMock->method('getNewToken')->willReturn(new AccessTokenEntity());
        $accessTokenRepositoryMock->expects($this->at(0))->method('persistNewAccessToken')->willThrowException(UniqueTokenIdentifierConstraintViolationException::create());
        $accessTokenRepositoryMock->expects($this->at(1))->method('persistNewAccessToken')->willReturnSelf();

        $grant = new ImplicitGrant(new \DateInterval('PT10M'));
        $grant->setPrivateKey(new CryptKey('file://' . __DIR__ . '/../Stubs/private.key'));
        $grant->setAccessTokenRepository($accessTokenRepositoryMock);

        $this->assertTrue($grant->completeAuthorizationRequest($authRequest) instanceof RedirectResponse);
    }

    /**
     * @throws OAuthServerException
     */
    public function testAccessTokenRepositoryFailToPersist()
    {
        $authRequest = new AuthorizationRequest();
        $authRequest->setAuthorizationApproved(true);
        $authRequest->setClient(new ClientEntity());
        $authRequest->setGrantTypeId('authorization_code');
        $authRequest->setUser(new UserEntity());

        /** @var AccessTokenRepositoryInterface|\PHPUnit_Framework_MockObject_MockObject $accessTokenRepositoryMock */
        $accessTokenRepositoryMock = $this->getMockBuilder(AccessTokenRepositoryInterface::class)->getMock();
        $accessTokenRepositoryMock->method('getNewToken')->willReturn(new AccessTokenEntity());
        $accessTokenRepositoryMock->method('persistNewAccessToken')->willThrowException(OAuthServerException::serverError('something bad happened'));

        $grant = new ImplicitGrant(new \DateInterval('PT10M'));
        $grant->setPrivateKey(new CryptKey('file://' . __DIR__ . '/../Stubs/private.key'));
        $grant->setAccessTokenRepository($accessTokenRepositoryMock);

        $this->expectException(OAuthServerException::class);
        $this->expectExceptionCode(7);
        $grant->completeAuthorizationRequest($authRequest);
    }

    /**
     * @throws OAuthServerException
     */
    public function testAccessTokenRepositoryFailToPersistUniqueNoInfiniteLoop()
    {
        $authRequest = new AuthorizationRequest();
        $authRequest->setAuthorizationApproved(true);
        $authRequest->setClient(new ClientEntity());
        $authRequest->setGrantTypeId('authorization_code');
        $authRequest->setUser(new UserEntity());

        /** @var AccessTokenRepositoryInterface|\PHPUnit_Framework_MockObject_MockObject $accessTokenRepositoryMock */
        $accessTokenRepositoryMock = $this->getMockBuilder(AccessTokenRepositoryInterface::class)->getMock();
        $accessTokenRepositoryMock->method('getNewToken')->willReturn(new AccessTokenEntity());
        $accessTokenRepositoryMock->method('persistNewAccessToken')->willThrowException(UniqueTokenIdentifierConstraintViolationException::create());

        $grant = new ImplicitGrant(new \DateInterval('PT10M'));
        $grant->setPrivateKey(new CryptKey('file://' . __DIR__ . '/../Stubs/private.key'));
        $grant->setAccessTokenRepository($accessTokenRepositoryMock);

        $this->expectException(UniqueTokenIdentifierConstraintViolationException::class);
        $this->expectExceptionCode(100);
        $grant->completeAuthorizationRequest($authRequest);
    }

    public function testSetRefreshTokenTTL()
    {
        $grant = new ImplicitGrant(new \DateInterval('PT10M'));
        $this->expectException(\LogicException::class);
        $grant->setRefreshTokenTTL(new \DateInterval('PT10M'));
    }

    public function testSetRefreshTokenRepository()
    {
        $grant = new ImplicitGrant(new \DateInterval('PT10M'));
        $refreshTokenRepositoryMock = $this->getMockBuilder(RefreshTokenRepositoryInterface::class)->getMock();
        $this->expectException(\LogicException::class);
        $grant->setRefreshTokenRepository($refreshTokenRepositoryMock);
    }

    /**
     * @throws OAuthServerException
     */
    public function testCompleteAuthorizationRequestNoUser()
    {
        $grant = new ImplicitGrant(new \DateInterval('PT10M'));
        $this->expectException(\LogicException::class);
        $grant->completeAuthorizationRequest(new AuthorizationRequest());
    }

    /**
     * @throws OAuthServerException
     */
    public function testValidateAuthorizationRequestFailsWithoutScope()
    {
        $client = new ClientEntity();
        $client->setRedirectUri('http://foo/bar');
        $clientRepositoryMock = $this->getMockBuilder(ClientRepositoryInterface::class)->getMock();
        $clientRepositoryMock->method('getClientEntity')->willReturn($client);

        $scopeRepositoryMock = $this->getMockBuilder(ScopeRepositoryInterface::class)->getMock();
        $scopeEntity = new ScopeEntity();
        $scopeRepositoryMock->method('getScopeEntityByIdentifier')->willReturn($scopeEntity);
        $scopeRepositoryMock->method('finalizeScopes')->willReturnArgument(0);

        $grant = new ImplicitGrant(new \DateInterval('PT10M'));
        $grant->setClientRepository($clientRepositoryMock);
        $grant->setScopeRepository($scopeRepositoryMock);

        $request = new Request([
            'response_type' => 'code',
            'client_id' => 'foo',
            'redirect_uri' => 'http://foo/bar',
        ]);

        $this->expectException(OAuthServerException::class);
        $this->expectExceptionCode(5);
        $grant->validateAuthorizationRequest($request);
    }
}
