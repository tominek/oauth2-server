<?php

namespace Tominek\OAuth2\Server\Tests\Grant;

use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Request;
use Tominek\OAuth2\Server\Entities\AccessTokenEntityInterface;
use Tominek\OAuth2\Server\Entities\AuthCodeEntityInterface;
use Tominek\OAuth2\Server\Entities\RefreshTokenEntityInterface;
use Tominek\OAuth2\Server\Event\Emitter;
use Tominek\OAuth2\Server\Exception\OAuthServerException;
use Tominek\OAuth2\Server\Grant\AbstractGrant;
use Tominek\OAuth2\Server\Repositories\AccessTokenRepositoryInterface;
use Tominek\OAuth2\Server\Repositories\AuthCodeRepositoryInterface;
use Tominek\OAuth2\Server\Repositories\ClientRepositoryInterface;
use Tominek\OAuth2\Server\Repositories\RefreshTokenRepositoryInterface;
use Tominek\OAuth2\Server\Repositories\ScopeRepositoryInterface;
use Tominek\OAuth2\Server\RequestTypes\AuthorizationRequest;
use Tominek\OAuth2\Server\Tests\Stubs\AccessTokenEntity;
use Tominek\OAuth2\Server\Tests\Stubs\AuthCodeEntity;
use Tominek\OAuth2\Server\Tests\Stubs\ClientEntity;
use Tominek\OAuth2\Server\Tests\Stubs\RefreshTokenEntity;
use Tominek\OAuth2\Server\Tests\Stubs\ScopeEntity;

class AbstractGrantTest extends TestCase
{
    /**
     * @Test
     */
    public function testGetSet()
    {
        /** @var AbstractGrant $grantMock */
        $grantMock = $this->getMockForAbstractClass(AbstractGrant::class);
        $grantMock->setEmitter(new Emitter());
    }

    /**
     * @Test
     */
    public function testHttpBasicWithPassword()
    {
        /** @var AbstractGrant $grantMock */
        $grantMock = $this->getMockForAbstractClass(AbstractGrant::class);
        $abstractGrantReflection = new \ReflectionClass($grantMock);

        $serverRequest = new Request();
        $serverRequest->headers->set('Authorization', 'Basic ' . base64_encode('Open:Sesame'));
        $basicAuthMethod = $abstractGrantReflection->getMethod('getBasicAuthCredentials');
        $basicAuthMethod->setAccessible(true);

        $this->assertSame(['Open', 'Sesame'], $basicAuthMethod->invoke($grantMock, $serverRequest));
    }

    /**
     * @Test
     */
    public function testHttpBasicNoPassword()
    {
        /** @var AbstractGrant $grantMock */
        $grantMock = $this->getMockForAbstractClass(AbstractGrant::class);
        $abstractGrantReflection = new \ReflectionClass($grantMock);

        $serverRequest = new Request();
        $serverRequest->headers->set('Authorization', 'Basic ' . base64_encode('Open:'));
        $basicAuthMethod = $abstractGrantReflection->getMethod('getBasicAuthCredentials');
        $basicAuthMethod->setAccessible(true);

        $this->assertSame(['Open', ''], $basicAuthMethod->invoke($grantMock, $serverRequest));
    }

    /**
     * @Test
     */
    public function testHttpBasicNotBasic()
    {
        /** @var AbstractGrant $grantMock */
        $grantMock = $this->getMockForAbstractClass(AbstractGrant::class);
        $abstractGrantReflection = new \ReflectionClass($grantMock);

        $serverRequest = new Request();
        $serverRequest->headers->set('Authorization', 'Foo ' . base64_encode('Open:Sesame'));
        $basicAuthMethod = $abstractGrantReflection->getMethod('getBasicAuthCredentials');
        $basicAuthMethod->setAccessible(true);

        $this->assertSame([null, null], $basicAuthMethod->invoke($grantMock, $serverRequest));
    }

    /**
     * @Test
     */
    public function testHttpBasicNotBase64()
    {
        /** @var AbstractGrant $grantMock */
        $grantMock = $this->getMockForAbstractClass(AbstractGrant::class);
        $abstractGrantReflection = new \ReflectionClass($grantMock);

        $serverRequest = new Request();
        $serverRequest->headers->set('Authorization', 'Basic ||');
        $basicAuthMethod = $abstractGrantReflection->getMethod('getBasicAuthCredentials');
        $basicAuthMethod->setAccessible(true);

        $this->assertSame([null, null], $basicAuthMethod->invoke($grantMock, $serverRequest));
    }

    /**
     * @Test
     */
    public function testHttpBasicNoColon()
    {
        /** @var AbstractGrant $grantMock */
        $grantMock = $this->getMockForAbstractClass(AbstractGrant::class);
        $abstractGrantReflection = new \ReflectionClass($grantMock);

        $serverRequest = new Request();
        $serverRequest->headers->set('Authorization', 'Basic ' . base64_encode('OpenSesame'));
        $basicAuthMethod = $abstractGrantReflection->getMethod('getBasicAuthCredentials');
        $basicAuthMethod->setAccessible(true);

        $this->assertSame([null, null], $basicAuthMethod->invoke($grantMock, $serverRequest));
    }

    /**
     * @Test
     */
    public function testValidateClientPublic()
    {
        $client = new ClientEntity();

        $clientRepositoryMock = $this->getMockBuilder(ClientRepositoryInterface::class)->getMock();
        $clientRepositoryMock->method('getClientEntity')->willReturn($client);

        /** @var AbstractGrant $grantMock */
        $grantMock = $this->getMockForAbstractClass(AbstractGrant::class);
        $grantMock->setClientRepository($clientRepositoryMock);

        $abstractGrantReflection = new \ReflectionClass($grantMock);

        $serverRequest = new Request([
            'client_id' => 'foo',
        ]);
        $validateClientMethod = $abstractGrantReflection->getMethod('validateClient');
        $validateClientMethod->setAccessible(true);

        $result = $validateClientMethod->invoke($grantMock, $serverRequest, true, true);
        $this->assertEquals($client, $result);
    }

    /**
     * @Test
     */
    public function testValidateClientConfidential()
    {
        $client = new ClientEntity();

        $clientRepositoryMock = $this->getMockBuilder(ClientRepositoryInterface::class)->getMock();
        $clientRepositoryMock->method('getClientEntity')->willReturn($client);

        /** @var AbstractGrant $grantMock */
        $grantMock = $this->getMockForAbstractClass(AbstractGrant::class);
        $grantMock->setClientRepository($clientRepositoryMock);

        $abstractGrantReflection = new \ReflectionClass($grantMock);

        $serverRequest = new Request([
            'client_id' => 'foo',
            'client_secret' => 'bar',
            'redirect_uri' => 'http://foo/bar',
        ]);
        $validateClientMethod = $abstractGrantReflection->getMethod('validateClient');
        $validateClientMethod->setAccessible(true);

        $result = $validateClientMethod->invoke($grantMock, $serverRequest, true, true);
        $this->assertEquals($client, $result);
    }

    /**
     * @Test
     */
    public function testValidateClientMissingClientId()
    {
        $client = new ClientEntity();
        $clientRepositoryMock = $this->getMockBuilder(ClientRepositoryInterface::class)->getMock();
        $clientRepositoryMock->method('getClientEntity')->willReturn($client);

        /** @var AbstractGrant $grantMock */
        $grantMock = $this->getMockForAbstractClass(AbstractGrant::class);
        $grantMock->setClientRepository($clientRepositoryMock);

        $abstractGrantReflection = new \ReflectionClass($grantMock);

        $serverRequest = new Request();
        $validateClientMethod = $abstractGrantReflection->getMethod('validateClient');
        $validateClientMethod->setAccessible(true);

        $this->expectException(OAuthServerException::class);
        $validateClientMethod->invoke($grantMock, $serverRequest, true, true);
//
    }

    /**
     * @Test
     */
    public function testValidateClientMissingClientSecret()
    {
        $clientRepositoryMock = $this->getMockBuilder(ClientRepositoryInterface::class)->getMock();
        $clientRepositoryMock->method('getClientEntity')->willReturn(null);

        /** @var AbstractGrant $grantMock */
        $grantMock = $this->getMockForAbstractClass(AbstractGrant::class);
        $grantMock->setClientRepository($clientRepositoryMock);

        $abstractGrantReflection = new \ReflectionClass($grantMock);

        $serverRequest = new Request([
            'client_id' => 'foo',
        ]);

        $validateClientMethod = $abstractGrantReflection->getMethod('validateClient');
        $validateClientMethod->setAccessible(true);

        $this->expectException(OAuthServerException::class);
        $validateClientMethod->invoke($grantMock, $serverRequest, true, true);
    }

    /**
     * @Test
     */
    public function testValidateClientInvalidClientSecret()
    {
        $clientRepositoryMock = $this->getMockBuilder(ClientRepositoryInterface::class)->getMock();
        $clientRepositoryMock->method('getClientEntity')->willReturn(null);

        /** @var AbstractGrant $grantMock */
        $grantMock = $this->getMockForAbstractClass(AbstractGrant::class);
        $grantMock->setClientRepository($clientRepositoryMock);

        $abstractGrantReflection = new \ReflectionClass($grantMock);

        $serverRequest = new Request([
            'client_id' => 'foo',
            'client_secret' => 'foo',
        ]);

        $validateClientMethod = $abstractGrantReflection->getMethod('validateClient');
        $validateClientMethod->setAccessible(true);

        $this->expectException(OAuthServerException::class);
        $validateClientMethod->invoke($grantMock, $serverRequest, true, true);
    }

    /**
     * @Test
     */
    public function testValidateClientInvalidRedirectUri()
    {
        $client = new ClientEntity();
        $client->setRedirectUri('http://foo/bar');
        $clientRepositoryMock = $this->getMockBuilder(ClientRepositoryInterface::class)->getMock();
        $clientRepositoryMock->method('getClientEntity')->willReturn($client);

        /** @var AbstractGrant $grantMock */
        $grantMock = $this->getMockForAbstractClass(AbstractGrant::class);
        $grantMock->setClientRepository($clientRepositoryMock);

        $abstractGrantReflection = new \ReflectionClass($grantMock);

        $serverRequest = new Request([
            'client_id' => 'foo',
            'redirect_uri' => 'http://bar/foo',
        ]);

        $validateClientMethod = $abstractGrantReflection->getMethod('validateClient');
        $validateClientMethod->setAccessible(true);

        $this->expectException(OAuthServerException::class);
        $validateClientMethod->invoke($grantMock, $serverRequest, true, true);
    }

    /**
     * @Test
     */
    public function testValidateClientInvalidRedirectUriArray()
    {
        $client = new ClientEntity();
        $client->setRedirectUri(['http://foo/bar']);
        $clientRepositoryMock = $this->getMockBuilder(ClientRepositoryInterface::class)->getMock();
        $clientRepositoryMock->method('getClientEntity')->willReturn($client);

        /** @var AbstractGrant $grantMock */
        $grantMock = $this->getMockForAbstractClass(AbstractGrant::class);
        $grantMock->setClientRepository($clientRepositoryMock);

        $abstractGrantReflection = new \ReflectionClass($grantMock);

        $serverRequest = new Request([
            'client_id' => 'foo',
            'redirect_uri' => 'http://bar/foo',
        ]);

        $validateClientMethod = $abstractGrantReflection->getMethod('validateClient');
        $validateClientMethod->setAccessible(true);

        $this->expectException(OAuthServerException::class);
        $validateClientMethod->invoke($grantMock, $serverRequest, true, true);
    }

    /**
     * @Test
     */
    public function testValidateClientBadClient()
    {
        $clientRepositoryMock = $this->getMockBuilder(ClientRepositoryInterface::class)->getMock();
        $clientRepositoryMock->method('getClientEntity')->willReturn(null);

        /** @var AbstractGrant $grantMock */
        $grantMock = $this->getMockForAbstractClass(AbstractGrant::class);
        $grantMock->setClientRepository($clientRepositoryMock);

        $abstractGrantReflection = new \ReflectionClass($grantMock);

        $serverRequest = new Request([
            'client_id' => 'foo',
            'client_secret' => 'bar',
        ]);

        $validateClientMethod = $abstractGrantReflection->getMethod('validateClient');
        $validateClientMethod->setAccessible(true);

        $this->expectException(OAuthServerException::class);
        $validateClientMethod->invoke($grantMock, $serverRequest, true);
    }

    /**
     * @Test
     */
    public function testCanRespondToRequest()
    {
        $grantMock = $this->getMockForAbstractClass(AbstractGrant::class);
        $grantMock->method('getIdentifier')->willReturn('foobar');

        $serverRequest = new Request([
            'grant_type' => 'foobar',
        ]);

        $this->assertTrue($grantMock->canRespondToAccessTokenRequest($serverRequest));
    }

    /**
     * @Test
     */
    public function testIssueRefreshToken()
    {
        $refreshTokenRepoMock = $this->getMockBuilder(RefreshTokenRepositoryInterface::class)->getMock();
        $refreshTokenRepoMock
            ->expects($this->once())
            ->method('getNewRefreshToken')
            ->willReturn(new RefreshTokenEntity());

        /** @var AbstractGrant $grantMock */
        $grantMock = $this->getMockForAbstractClass(AbstractGrant::class);
        $grantMock->setRefreshTokenTTL(new \DateInterval('PT1M'));
        $grantMock->setRefreshTokenRepository($refreshTokenRepoMock);

        $abstractGrantReflection = new \ReflectionClass($grantMock);
        $issueRefreshTokenMethod = $abstractGrantReflection->getMethod('issueRefreshToken');
        $issueRefreshTokenMethod->setAccessible(true);

        $accessToken = new AccessTokenEntity();
        /** @var RefreshTokenEntityInterface $refreshToken */
        $refreshToken = $issueRefreshTokenMethod->invoke($grantMock, $accessToken);
        $this->assertTrue($refreshToken instanceof RefreshTokenEntityInterface);
        $this->assertEquals($accessToken, $refreshToken->getAccessToken());
    }

    /**
     * @Test
     */
    public function testIssueAccessToken()
    {
        $accessTokenRepoMock = $this->getMockBuilder(AccessTokenRepositoryInterface::class)->getMock();
        $accessTokenRepoMock->method('getNewToken')->willReturn(new AccessTokenEntity());

        /** @var AbstractGrant $grantMock */
        $grantMock = $this->getMockForAbstractClass(AbstractGrant::class);
        $grantMock->setAccessTokenRepository($accessTokenRepoMock);

        $abstractGrantReflection = new \ReflectionClass($grantMock);
        $issueAccessTokenMethod = $abstractGrantReflection->getMethod('issueAccessToken');
        $issueAccessTokenMethod->setAccessible(true);

        /** @var AccessTokenEntityInterface $accessToken */
        $accessToken = $issueAccessTokenMethod->invoke(
            $grantMock,
            new \DateInterval('PT1H'),
            new ClientEntity(),
            123,
            [new ScopeEntity()]
        );
        $this->assertTrue($accessToken instanceof AccessTokenEntityInterface);
    }

    /**
     * @Test
     */
    public function testIssueAuthCode()
    {
        $authCodeRepoMock = $this->getMockBuilder(AuthCodeRepositoryInterface::class)->getMock();
        $authCodeRepoMock->expects($this->once())->method('getNewAuthCode')->willReturn(new AuthCodeEntity());

        /** @var AbstractGrant $grantMock */
        $grantMock = $this->getMockForAbstractClass(AbstractGrant::class);
        $grantMock->setAuthCodeRepository($authCodeRepoMock);

        $abstractGrantReflection = new \ReflectionClass($grantMock);
        $issueAuthCodeMethod = $abstractGrantReflection->getMethod('issueAuthCode');
        $issueAuthCodeMethod->setAccessible(true);

        $this->assertTrue(
            $issueAuthCodeMethod->invoke(
                $grantMock,
                new \DateInterval('PT1H'),
                new ClientEntity(),
                123,
                'http://foo/bar',
                [new ScopeEntity()]
            ) instanceof AuthCodeEntityInterface
        );
    }

    /**
     * @Test
     */
    public function testGetCookieParameter()
    {
        $grantMock = $this->getMockForAbstractClass(AbstractGrant::class);
        $grantMock->method('getIdentifier')->willReturn('foobar');

        $abstractGrantReflection = new \ReflectionClass($grantMock);
        $method = $abstractGrantReflection->getMethod('getCookieParameter');
        $method->setAccessible(true);

        $serverRequest = new Request([], [], [], [
            'foo' => 'bar',
        ]);

        $this->assertEquals('bar', $method->invoke($grantMock, 'foo', $serverRequest));
        $this->assertEquals('foo', $method->invoke($grantMock, 'bar', $serverRequest, 'foo'));
    }

    /**
     * @Test
     */
    public function testGetQueryStringParameter()
    {
        $grantMock = $this->getMockForAbstractClass(AbstractGrant::class);
        $grantMock->method('getIdentifier')->willReturn('foobar');

        $abstractGrantReflection = new \ReflectionClass($grantMock);
        $method = $abstractGrantReflection->getMethod('getQueryStringParameter');
        $method->setAccessible(true);

        $serverRequest = new Request([
            'foo' => 'bar',
        ]);

        $this->assertEquals('bar', $method->invoke($grantMock, 'foo', $serverRequest));
        $this->assertEquals('foo', $method->invoke($grantMock, 'bar', $serverRequest, 'foo'));
    }

    /**
     * @Test
     * @throws OAuthServerException
     */
    public function testValidateScopes()
    {
        $scope = new ScopeEntity();
        $scopeRepositoryMock = $this->getMockBuilder(ScopeRepositoryInterface::class)->getMock();
        $scopeRepositoryMock->method('getScopeEntityByIdentifier')->willReturn($scope);

        /** @var AbstractGrant $grantMock */
        $grantMock = $this->getMockForAbstractClass(AbstractGrant::class);
        $grantMock->setScopeRepository($scopeRepositoryMock);

        $this->assertEquals([$scope], $grantMock->validateScopes('basic   '));
    }

    /**
     * @Test
     * @throws OAuthServerException
     */
    public function testValidateScopesBadScope()
    {
        $scopeRepositoryMock = $this->getMockBuilder(ScopeRepositoryInterface::class)->getMock();
        $scopeRepositoryMock->method('getScopeEntityByIdentifier')->willReturn(null);

        /** @var AbstractGrant $grantMock */
        $grantMock = $this->getMockForAbstractClass(AbstractGrant::class);
        $grantMock->setScopeRepository($scopeRepositoryMock);

        $this->expectException(OAuthServerException::class);
        $grantMock->validateScopes('basic   ');
    }

    /**
     * @Test
     */
    public function testGenerateUniqueIdentifier()
    {
        $grantMock = $this->getMockForAbstractClass(AbstractGrant::class);

        $abstractGrantReflection = new \ReflectionClass($grantMock);
        $method = $abstractGrantReflection->getMethod('generateUniqueIdentifier');
        $method->setAccessible(true);

        $this->assertTrue(is_string($method->invoke($grantMock)));
    }

    /**
     * @Test
     */
    public function testCanRespondToAuthorizationRequest()
    {
        $grantMock = $this->getMockForAbstractClass(AbstractGrant::class);
        $this->assertFalse($grantMock->canRespondToAuthorizationRequest(new Request()));
    }

    /**
     * @Test
     */
    public function testValidateAuthorizationRequest()
    {
        $grantMock = $this->getMockForAbstractClass(AbstractGrant::class);
        $this->expectException(\LogicException::class);
        $grantMock->validateAuthorizationRequest(new Request());
    }

    /**
     * @Test
     * @expectedException \LogicException
     */
    public function testCompleteAuthorizationRequest()
    {
        $grantMock = $this->getMockForAbstractClass(AbstractGrant::class);
        $this->expectException(\LogicException::class);
        $grantMock->completeAuthorizationRequest(new AuthorizationRequest());
    }
}
