<?php

namespace Tominek\OAuth2\Server\Tests\Grant;

use Symfony\Component\HttpFoundation\Request;
use Tominek\OAuth2\Server\Exception\OAuthServerException;
use Tominek\OAuth2\Server\Tests\Stubs\AccessTokenEntity;
use Tominek\OAuth2\Server\Tests\Stubs\ClientEntity;
use Tominek\OAuth2\Server\Tests\Stubs\CryptTraitStub;
use Tominek\OAuth2\Server\Tests\Stubs\RefreshTokenEntity;
use Tominek\OAuth2\Server\Tests\Stubs\ScopeEntity;
use Tominek\OAuth2\Server\Tests\Stubs\StubResponseType;
use PHPUnit\Framework\TestCase;
use Tominek\OAuth2\Server\CryptKey;
use Tominek\OAuth2\Server\Entities\AccessTokenEntityInterface;
use Tominek\OAuth2\Server\Entities\RefreshTokenEntityInterface;
use Tominek\OAuth2\Server\Grant\RefreshTokenGrant;
use Tominek\OAuth2\Server\Repositories\AccessTokenRepositoryInterface;
use Tominek\OAuth2\Server\Repositories\ClientRepositoryInterface;
use Tominek\OAuth2\Server\Repositories\RefreshTokenRepositoryInterface;
use Tominek\OAuth2\Server\Repositories\ScopeRepositoryInterface;

class RefreshTokenGrantTest extends TestCase
{
    /**
     * @var CryptTraitStub
     */
    protected $cryptStub;

    public function setUp()
    {
        $this->cryptStub = new CryptTraitStub();
    }

    public function testGetIdentifier()
    {
        $refreshTokenRepositoryMock = $this->getMockBuilder(RefreshTokenRepositoryInterface::class)->getMock();

        $grant = new RefreshTokenGrant($refreshTokenRepositoryMock);
        $this->assertEquals('refresh_token', $grant->getIdentifier());
    }

    /**
     * @throws OAuthServerException
     */
    public function testRespondToRequest()
    {
        $client = new ClientEntity();
        $client->setIdentifier('foo');
        $clientRepositoryMock = $this->getMockBuilder(ClientRepositoryInterface::class)->getMock();
        $clientRepositoryMock->method('getClientEntity')->willReturn($client);

        $scopeEntity = new ScopeEntity();
        $scopeEntity->setIdentifier('foo');
        $scopeRepositoryMock = $this->getMockBuilder(ScopeRepositoryInterface::class)->getMock();
        $scopeRepositoryMock->method('getScopeEntityByIdentifier')->willReturn($scopeEntity);

        $accessTokenRepositoryMock = $this->getMockBuilder(AccessTokenRepositoryInterface::class)->getMock();
        $accessTokenRepositoryMock->method('getNewToken')->willReturn(new AccessTokenEntity());
        $accessTokenRepositoryMock->expects($this->once())->method('persistNewAccessToken')->willReturnSelf();

        $refreshTokenRepositoryMock = $this->getMockBuilder(RefreshTokenRepositoryInterface::class)->getMock();
        $refreshTokenRepositoryMock->method('getNewRefreshToken')->willReturn(new RefreshTokenEntity());
        $refreshTokenRepositoryMock->expects($this->once())->method('persistNewRefreshToken')->willReturnSelf();

        $grant = new RefreshTokenGrant($refreshTokenRepositoryMock);
        $grant->setClientRepository($clientRepositoryMock);
        $grant->setScopeRepository($scopeRepositoryMock);
        $grant->setAccessTokenRepository($accessTokenRepositoryMock);
        $grant->setEncryptionKey($this->cryptStub->getKey());
        $grant->setPrivateKey(new CryptKey('file://' . __DIR__ . '/../Stubs/private.key'));

        $oldRefreshToken = $this->cryptStub->doEncrypt(
            json_encode(
                [
                    'client_id' => 'foo',
                    'refresh_token_id' => 'zyxwvu',
                    'access_token_id' => 'abcdef',
                    'scopes' => ['foo'],
                    'user_id' => 123,
                    'expire_time' => time() + 3600,
                ]
            )
        );

        $serverRequest = new Request([
            'client_id' => 'foo',
            'client_secret' => 'bar',
            'refresh_token' => $oldRefreshToken,
            'scopes' => ['foo'],
        ]);

        $responseType = new StubResponseType();
        $grant->respondToAccessTokenRequest($serverRequest, $responseType, new \DateInterval('PT5M'));

        $this->assertTrue($responseType->getAccessToken() instanceof AccessTokenEntityInterface);
        $this->assertTrue($responseType->getRefreshToken() instanceof RefreshTokenEntityInterface);
    }

    /**
     * @throws OAuthServerException
     */
    public function testRespondToReducedScopes()
    {
        $client = new ClientEntity();
        $client->setIdentifier('foo');
        $clientRepositoryMock = $this->getMockBuilder(ClientRepositoryInterface::class)->getMock();
        $clientRepositoryMock->method('getClientEntity')->willReturn($client);

        $accessTokenRepositoryMock = $this->getMockBuilder(AccessTokenRepositoryInterface::class)->getMock();
        $accessTokenRepositoryMock->method('getNewToken')->willReturn(new AccessTokenEntity());
        $accessTokenRepositoryMock->method('persistNewAccessToken')->willReturnSelf();

        $refreshTokenRepositoryMock = $this->getMockBuilder(RefreshTokenRepositoryInterface::class)->getMock();
        $refreshTokenRepositoryMock->method('persistNewRefreshToken')->willReturnSelf();
        $refreshTokenRepositoryMock->method('getNewRefreshToken')->willReturn(new RefreshTokenEntity());

        $scope = new ScopeEntity();
        $scope->setIdentifier('foo');
        $scopeRepositoryMock = $this->getMockBuilder(ScopeRepositoryInterface::class)->getMock();
        $scopeRepositoryMock->method('getScopeEntityByIdentifier')->willReturn($scope);

        $grant = new RefreshTokenGrant($refreshTokenRepositoryMock);
        $grant->setClientRepository($clientRepositoryMock);
        $grant->setAccessTokenRepository($accessTokenRepositoryMock);
        $grant->setScopeRepository($scopeRepositoryMock);
        $grant->setEncryptionKey($this->cryptStub->getKey());
        $grant->setPrivateKey(new CryptKey('file://' . __DIR__ . '/../Stubs/private.key'));

        $oldRefreshToken = $this->cryptStub->doEncrypt(
            json_encode(
                [
                    'client_id' => 'foo',
                    'refresh_token_id' => 'zyxwvu',
                    'access_token_id' => 'abcdef',
                    'scopes' => ['foo', 'bar'],
                    'user_id' => 123,
                    'expire_time' => time() + 3600,
                ]
            )
        );

        $serverRequest = new Request([
            'client_id' => 'foo',
            'client_secret' => 'bar',
            'refresh_token' => $oldRefreshToken,
            'scope' => 'foo',
        ]);

        $responseType = new StubResponseType();
        $grant->respondToAccessTokenRequest($serverRequest, $responseType, new \DateInterval('PT5M'));

        $this->assertTrue($responseType->getAccessToken() instanceof AccessTokenEntityInterface);
        $this->assertTrue($responseType->getRefreshToken() instanceof RefreshTokenEntityInterface);
    }

    /**
     * @throws OAuthServerException
     */
    public function testRespondToUnexpectedScope()
    {
        $client = new ClientEntity();
        $client->setIdentifier('foo');
        $clientRepositoryMock = $this->getMockBuilder(ClientRepositoryInterface::class)->getMock();
        $clientRepositoryMock->method('getClientEntity')->willReturn($client);

        $accessTokenRepositoryMock = $this->getMockBuilder(AccessTokenRepositoryInterface::class)->getMock();
        $accessTokenRepositoryMock->method('persistNewAccessToken')->willReturnSelf();

        $refreshTokenRepositoryMock = $this->getMockBuilder(RefreshTokenRepositoryInterface::class)->getMock();
        $refreshTokenRepositoryMock->method('persistNewRefreshToken')->willReturnSelf();

        $scope = new ScopeEntity();
        $scope->setIdentifier('foobar');
        $scopeRepositoryMock = $this->getMockBuilder(ScopeRepositoryInterface::class)->getMock();
        $scopeRepositoryMock->method('getScopeEntityByIdentifier')->willReturn($scope);

        $grant = new RefreshTokenGrant($refreshTokenRepositoryMock);
        $grant->setClientRepository($clientRepositoryMock);
        $grant->setAccessTokenRepository($accessTokenRepositoryMock);
        $grant->setScopeRepository($scopeRepositoryMock);
        $grant->setEncryptionKey($this->cryptStub->getKey());
        $grant->setPrivateKey(new CryptKey('file://' . __DIR__ . '/../Stubs/private.key'));

        $oldRefreshToken = $this->cryptStub->doEncrypt(
            json_encode(
                [
                    'client_id' => 'foo',
                    'refresh_token_id' => 'zyxwvu',
                    'access_token_id' => 'abcdef',
                    'scopes' => ['foo', 'bar'],
                    'user_id' => 123,
                    'expire_time' => time() + 3600,
                ]
            )
        );

        $serverRequest = new Request([
            'client_id' => 'foo',
            'client_secret' => 'bar',
            'refresh_token' => $oldRefreshToken,
            'scope' => 'foobar',
        ]);

        $responseType = new StubResponseType();
        $this->expectException(OAuthServerException::class);
        $this->expectExceptionCode(5);
        $grant->respondToAccessTokenRequest($serverRequest, $responseType, new \DateInterval('PT5M'));
    }

    /**
     * @throws OAuthServerException
     */
    public function testRespondToRequestMissingOldToken()
    {
        $client = new ClientEntity();
        $client->setIdentifier('foo');
        $clientRepositoryMock = $this->getMockBuilder(ClientRepositoryInterface::class)->getMock();
        $clientRepositoryMock->method('getClientEntity')->willReturn($client);

        $accessTokenRepositoryMock = $this->getMockBuilder(AccessTokenRepositoryInterface::class)->getMock();
        $refreshTokenRepositoryMock = $this->getMockBuilder(RefreshTokenRepositoryInterface::class)->getMock();

        $grant = new RefreshTokenGrant($refreshTokenRepositoryMock);
        $grant->setClientRepository($clientRepositoryMock);
        $grant->setAccessTokenRepository($accessTokenRepositoryMock);
        $grant->setEncryptionKey($this->cryptStub->getKey());
        $grant->setPrivateKey(new CryptKey('file://' . __DIR__ . '/../Stubs/private.key'));

        $serverRequest = new Request([
            'client_id' => 'foo',
            'client_secret' => 'bar',
        ]);

        $responseType = new StubResponseType();
        $this->expectException(OAuthServerException::class);
        $this->expectExceptionCode(3);
        $grant->respondToAccessTokenRequest($serverRequest, $responseType, new \DateInterval('PT5M'));
    }

    /**
     * @throws OAuthServerException
     */
    public function testRespondToRequestInvalidOldToken()
    {
        $client = new ClientEntity();
        $client->setIdentifier('foo');
        $clientRepositoryMock = $this->getMockBuilder(ClientRepositoryInterface::class)->getMock();
        $clientRepositoryMock->method('getClientEntity')->willReturn($client);

        $accessTokenRepositoryMock = $this->getMockBuilder(AccessTokenRepositoryInterface::class)->getMock();
        $refreshTokenRepositoryMock = $this->getMockBuilder(RefreshTokenRepositoryInterface::class)->getMock();

        $grant = new RefreshTokenGrant($refreshTokenRepositoryMock);
        $grant->setClientRepository($clientRepositoryMock);
        $grant->setAccessTokenRepository($accessTokenRepositoryMock);
        $grant->setEncryptionKey($this->cryptStub->getKey());
        $grant->setPrivateKey(new CryptKey('file://' . __DIR__ . '/../Stubs/private.key'));

        $oldRefreshToken = 'foobar';

        $serverRequest = new Request([
            'client_id' => 'foo',
            'client_secret' => 'bar',
            'refresh_token' => $oldRefreshToken,
        ]);

        $responseType = new StubResponseType();
        $this->expectException(OAuthServerException::class);
        $this->expectExceptionCode(8);
        $grant->respondToAccessTokenRequest($serverRequest, $responseType, new \DateInterval('PT5M'));
    }

    /**
     * @throws OAuthServerException
     */
    public function testRespondToRequestClientMismatch()
    {
        $client = new ClientEntity();
        $client->setIdentifier('foo');
        $clientRepositoryMock = $this->getMockBuilder(ClientRepositoryInterface::class)->getMock();
        $clientRepositoryMock->method('getClientEntity')->willReturn($client);

        $accessTokenRepositoryMock = $this->getMockBuilder(AccessTokenRepositoryInterface::class)->getMock();
        $accessTokenRepositoryMock->method('persistNewAccessToken')->willReturnSelf();

        $refreshTokenRepositoryMock = $this->getMockBuilder(RefreshTokenRepositoryInterface::class)->getMock();
        $refreshTokenRepositoryMock->method('persistNewRefreshToken')->willReturnSelf();

        $grant = new RefreshTokenGrant($refreshTokenRepositoryMock);
        $grant->setClientRepository($clientRepositoryMock);
        $grant->setAccessTokenRepository($accessTokenRepositoryMock);
        $grant->setEncryptionKey($this->cryptStub->getKey());
        $grant->setPrivateKey(new CryptKey('file://' . __DIR__ . '/../Stubs/private.key'));

        $oldRefreshToken = $this->cryptStub->doEncrypt(
            json_encode(
                [
                    'client_id' => 'bar',
                    'refresh_token_id' => 'zyxwvu',
                    'access_token_id' => 'abcdef',
                    'scopes' => ['foo'],
                    'user_id' => 123,
                    'expire_time' => time() + 3600,
                ]
            )
        );

        $serverRequest = new Request([
            'client_id' => 'foo',
            'client_secret' => 'bar',
            'refresh_token' => $oldRefreshToken,
        ]);

        $responseType = new StubResponseType();
        $this->expectException(OAuthServerException::class);
        $this->expectExceptionCode(8);
        $grant->respondToAccessTokenRequest($serverRequest, $responseType, new \DateInterval('PT5M'));
    }

    /**
     * @throws OAuthServerException
     */
    public function testRespondToRequestExpiredToken()
    {
        $client = new ClientEntity();
        $client->setIdentifier('foo');
        $clientRepositoryMock = $this->getMockBuilder(ClientRepositoryInterface::class)->getMock();
        $clientRepositoryMock->method('getClientEntity')->willReturn($client);

        $accessTokenRepositoryMock = $this->getMockBuilder(AccessTokenRepositoryInterface::class)->getMock();
        $refreshTokenRepositoryMock = $this->getMockBuilder(RefreshTokenRepositoryInterface::class)->getMock();

        $grant = new RefreshTokenGrant($refreshTokenRepositoryMock);
        $grant->setClientRepository($clientRepositoryMock);
        $grant->setAccessTokenRepository($accessTokenRepositoryMock);
        $grant->setEncryptionKey($this->cryptStub->getKey());
        $grant->setPrivateKey(new CryptKey('file://' . __DIR__ . '/../Stubs/private.key'));

        $oldRefreshToken = $this->cryptStub->doEncrypt(
            json_encode(
                [
                    'client_id' => 'foo',
                    'refresh_token_id' => 'zyxwvu',
                    'access_token_id' => 'abcdef',
                    'scopes' => ['foo'],
                    'user_id' => 123,
                    'expire_time' => time() - 3600,
                ]
            )
        );

        $serverRequest = new Request([
            'client_id' => 'foo',
            'client_secret' => 'bar',
            'refresh_token' => $oldRefreshToken,
        ]);

        $responseType = new StubResponseType();
        $this->expectException(OAuthServerException::class);
        $this->expectExceptionCode(8);
        $grant->respondToAccessTokenRequest($serverRequest, $responseType, new \DateInterval('PT5M'));
    }

    /**
     * @throws OAuthServerException
     */
    public function testRespondToRequestRevokedToken()
    {
        $client = new ClientEntity();
        $client->setIdentifier('foo');
        $clientRepositoryMock = $this->getMockBuilder(ClientRepositoryInterface::class)->getMock();
        $clientRepositoryMock->method('getClientEntity')->willReturn($client);

        $accessTokenRepositoryMock = $this->getMockBuilder(AccessTokenRepositoryInterface::class)->getMock();
        $refreshTokenRepositoryMock = $this->getMockBuilder(RefreshTokenRepositoryInterface::class)->getMock();
        $refreshTokenRepositoryMock->method('isRefreshTokenRevoked')->willReturn(true);

        $grant = new RefreshTokenGrant($refreshTokenRepositoryMock);
        $grant->setClientRepository($clientRepositoryMock);
        $grant->setAccessTokenRepository($accessTokenRepositoryMock);
        $grant->setEncryptionKey($this->cryptStub->getKey());
        $grant->setPrivateKey(new CryptKey('file://' . __DIR__ . '/../Stubs/private.key'));

        $oldRefreshToken = $this->cryptStub->doEncrypt(
            json_encode(
                [
                    'client_id' => 'foo',
                    'refresh_token_id' => 'zyxwvu',
                    'access_token_id' => 'abcdef',
                    'scopes' => ['foo'],
                    'user_id' => 123,
                    'expire_time' => time() + 3600,
                ]
            )
        );

        $serverRequest = new Request([
            'client_id' => 'foo',
            'client_secret' => 'bar',
            'refresh_token' => $oldRefreshToken,
        ]);

        $responseType = new StubResponseType();
        $this->expectException(OAuthServerException::class);
        $this->expectExceptionCode(8);
        $grant->respondToAccessTokenRequest($serverRequest, $responseType, new \DateInterval('PT5M'));
    }
}
