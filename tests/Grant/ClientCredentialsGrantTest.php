<?php

namespace Tominek\OAuth2\Server\Tests\Grant;

use Symfony\Component\HttpFoundation\Request;
use Tominek\OAuth2\Server\Entities\AccessTokenEntityInterface;
use Tominek\OAuth2\Server\Exception\OAuthServerException;
use Tominek\OAuth2\Server\Grant\ClientCredentialsGrant;
use Tominek\OAuth2\Server\Repositories\AccessTokenRepositoryInterface;
use Tominek\OAuth2\Server\Repositories\ClientRepositoryInterface;
use Tominek\OAuth2\Server\Repositories\ScopeRepositoryInterface;
use Tominek\OAuth2\Server\Tests\Stubs\AccessTokenEntity;
use Tominek\OAuth2\Server\Tests\Stubs\ClientEntity;
use Tominek\OAuth2\Server\Tests\Stubs\ScopeEntity;
use Tominek\OAuth2\Server\Tests\Stubs\StubResponseType;
use PHPUnit\Framework\TestCase;

class ClientCredentialsGrantTest extends TestCase
{
    const DEFAULT_SCOPE = 'basic';

    public function testGetIdentifier()
    {
        $grant = new ClientCredentialsGrant();
        $this->assertEquals('client_credentials', $grant->getIdentifier());
    }

    public function testRespondToRequest()
    {
        $client = new ClientEntity();
        $clientRepositoryMock = $this->getMockBuilder(ClientRepositoryInterface::class)->getMock();
        $clientRepositoryMock->method('getClientEntity')->willReturn($client);

        $accessTokenRepositoryMock = $this->getMockBuilder(AccessTokenRepositoryInterface::class)->getMock();
        $accessTokenRepositoryMock->method('getNewToken')->willReturn(new AccessTokenEntity());
        $accessTokenRepositoryMock->method('persistNewAccessToken')->willReturnSelf();

        $scope = new ScopeEntity();
        $scopeRepositoryMock = $this->getMockBuilder(ScopeRepositoryInterface::class)->getMock();
        $scopeRepositoryMock->method('getScopeEntityByIdentifier')->willReturn($scope);
        $scopeRepositoryMock->method('finalizeScopes')->willReturnArgument(0);

        $grant = new ClientCredentialsGrant();
        $grant->setClientRepository($clientRepositoryMock);
        $grant->setAccessTokenRepository($accessTokenRepositoryMock);
        $grant->setScopeRepository($scopeRepositoryMock);
        $grant->setDefaultScope(self::DEFAULT_SCOPE);

        $serverRequest = new Request([
            'client_id' => 'foo',
            'client_secret' => 'bar',
        ]);

        $responseType = new StubResponseType();
        $grant->respondToAccessTokenRequest($serverRequest, $responseType, new \DateInterval('PT5M'));

        $this->assertTrue($responseType->getAccessToken() instanceof AccessTokenEntityInterface);
    }

    public function testRespondToRequestFailsWithoutScope()
    {
        $client = new ClientEntity();
        $clientRepositoryMock = $this->getMockBuilder(ClientRepositoryInterface::class)->getMock();
        $clientRepositoryMock->method('getClientEntity')->willReturn($client);

        $accessTokenRepositoryMock = $this->getMockBuilder(AccessTokenRepositoryInterface::class)->getMock();
        $accessTokenRepositoryMock->method('getNewToken')->willReturn(new AccessTokenEntity());
        $accessTokenRepositoryMock->method('persistNewAccessToken')->willReturnSelf();

        $scope = new ScopeEntity();
        $scopeRepositoryMock = $this->getMockBuilder(ScopeRepositoryInterface::class)->getMock();
        $scopeRepositoryMock->method('getScopeEntityByIdentifier')->willReturn($scope);
        $scopeRepositoryMock->method('finalizeScopes')->willReturnArgument(0);

        $grant = new ClientCredentialsGrant();
        $grant->setClientRepository($clientRepositoryMock);
        $grant->setAccessTokenRepository($accessTokenRepositoryMock);
        $grant->setScopeRepository($scopeRepositoryMock);

        $serverRequest = new Request([
            'client_id' => 'foo',
            'client_secret' => 'bar',
        ]);

        $responseType = new StubResponseType();
        $this->expectException(OAuthServerException::class);
        $this->expectExceptionCode(5);
        $grant->respondToAccessTokenRequest($serverRequest, $responseType, new \DateInterval('PT5M'));
    }
}