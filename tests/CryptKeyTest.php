<?php

namespace Tominek\OAuth2\Server\Tests\Utils;

use PHPUnit\Framework\TestCase;
use Tominek\OAuth2\Server\CryptKey;

class CryptKeyTest extends TestCase
{
    public function testNoFile()
    {
        $this->expectException(\LogicException::class);
        new CryptKey('undefined file');
    }

    public function testKeyCreation()
    {
        $keyFile = __DIR__ . '/Stubs/public.key';
        $key = new CryptKey($keyFile, 'secret');

        $this->assertEquals('file://' . $keyFile, $key->getKeyPath());
        $this->assertEquals('secret', $key->getPassPhrase());
    }

    /**
     * TODO Fix test
     */
//    public function testKeyFileCreation()
//    {
//        $keyContent = file_get_contents(__DIR__ . '/Stubs/public.key');
//        $key = new CryptKey($keyContent);
//
//        $this->assertEquals(
//            'file://' . sys_get_temp_dir() . '/' . sha1($keyContent) . '.key',
//            $key->getKeyPath()
//        );
//    }
}
