<?php

namespace Tominek\OAuth2\Server\Tests\ResponseTypes;

use Tominek\OAuth2\Server\Entities\AccessTokenEntityInterface;
use Tominek\OAuth2\Server\ResponseTypes\BearerTokenResponse;

class BearerTokenResponseWithParams extends BearerTokenResponse
{
    protected function getExtraParams(AccessTokenEntityInterface $accessToken)
    {
        return ['foo' => 'bar', 'token_type' => 'Should not overwrite'];
    }
}
