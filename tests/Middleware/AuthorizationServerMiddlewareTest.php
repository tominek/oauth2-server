<?php

namespace Tominek\OAuth2\Server\Tests\Middleware;

use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Tominek\OAuth2\Server\AuthorizationServer;
use Tominek\OAuth2\Server\Exception\OAuthServerException;
use Tominek\OAuth2\Server\Grant\ClientCredentialsGrant;
use Tominek\OAuth2\Server\Middleware\AuthorizationServerMiddleware;
use Tominek\OAuth2\Server\Repositories\AccessTokenRepositoryInterface;
use Tominek\OAuth2\Server\Repositories\ClientRepositoryInterface;
use Tominek\OAuth2\Server\Repositories\ScopeRepositoryInterface;
use Tominek\OAuth2\Server\Tests\Stubs\AccessTokenEntity;
use Tominek\OAuth2\Server\Tests\Stubs\ClientEntity;
use Tominek\OAuth2\Server\Tests\Stubs\ScopeEntity;
use Tominek\OAuth2\Server\Tests\Stubs\StubResponseType;

class AuthorizationServerMiddlewareTest extends TestCase
{
    const DEFAULT_SCOPE = 'basic';

    public function testValidResponse()
    {
        $clientRepository = $this->getMockBuilder(ClientRepositoryInterface::class)->getMock();
        $clientRepository->method('getClientEntity')->willReturn(new ClientEntity());

        $scopeEntity = new ScopeEntity;
        $scopeRepositoryMock = $this->getMockBuilder(ScopeRepositoryInterface::class)->getMock();
        $scopeRepositoryMock->method('getScopeEntityByIdentifier')->willReturn($scopeEntity);
        $scopeRepositoryMock->method('finalizeScopes')->willReturnArgument(0);

        $accessRepositoryMock = $this->getMockBuilder(AccessTokenRepositoryInterface::class)->getMock();
        $accessRepositoryMock->method('getNewToken')->willReturn(new AccessTokenEntity());

        $server = new AuthorizationServer(
            $clientRepository,
            $accessRepositoryMock,
            $scopeRepositoryMock,
            'file://' . __DIR__ . '/../Stubs/private.key',
            base64_encode(random_bytes(36)),
            new StubResponseType()
        );

        $server->setDefaultScope(self::DEFAULT_SCOPE);
        $server->enableGrantType(new ClientCredentialsGrant());

        $request = new Request([
            'grant_type' => 'client_credentials',
            'client_id' => 'foo',
            'client_secret' => 'bar'
        ]);

        $middleware = new AuthorizationServerMiddleware($server);
        $response = $middleware->__invoke(
            $request,
            new Response(),
            function () {
                return func_get_args()[1];
            }
        );
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testOAuthErrorResponse()
    {
        $clientRepository = $this->getMockBuilder(ClientRepositoryInterface::class)->getMock();
        $clientRepository->method('getClientEntity')->willReturn(null);

        $server = new AuthorizationServer(
            $clientRepository,
            $this->getMockBuilder(AccessTokenRepositoryInterface::class)->getMock(),
            $this->getMockBuilder(ScopeRepositoryInterface::class)->getMock(),
            'file://' . __DIR__ . '/../Stubs/private.key',
            base64_encode(random_bytes(36)),
            new StubResponseType()
        );

        $server->enableGrantType(new ClientCredentialsGrant(), new \DateInterval('PT1M'));

        $request = new Request([
            'grant_type' => 'client_credentials',
            'client_id' => 'foo',
            'client_secret' => 'bar'
        ]);

        $middleware = new AuthorizationServerMiddleware($server);

        $response = $middleware->__invoke(
            $request,
            new Response(),
            function () {
                return func_get_args()[1];
            }
        );

        $this->assertEquals(401, $response->getStatusCode());
    }

    public function testOAuthErrorResponseRedirectUri()
    {
        $exception = OAuthServerException::invalidScope('test', 'http://foo/bar');
        $response = $exception->generateHttpResponse();

        $this->assertEquals(302, $response->getStatusCode());
        $this->assertEquals('http://foo/bar?error=invalid_scope&message=The+requested+scope+is+invalid%2C+unknown%2C+or+malformed&hint=Check+the+%60test%60+scope',
            $response->headers->get('location'));
    }

    public function testOAuthErrorResponseRedirectUriFragment()
    {
        $exception = OAuthServerException::invalidScope('test', 'http://foo/bar');
        $response = $exception->generateHttpResponse(true);

        $this->assertEquals(302, $response->getStatusCode());
        $this->assertEquals('http://foo/bar#error=invalid_scope&message=The+requested+scope+is+invalid%2C+unknown%2C+or+malformed&hint=Check+the+%60test%60+scope',
            $response->headers->get('location'));
    }
}
