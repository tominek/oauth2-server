<?php

namespace Tominek\OAuth2\Server\Tests\Stubs;

use Tominek\OAuth2\Server\Entities\RefreshTokenEntityInterface;
use Tominek\OAuth2\Server\Entities\Traits\EntityTrait;
use Tominek\OAuth2\Server\Entities\Traits\RefreshTokenTrait;

class RefreshTokenEntity implements RefreshTokenEntityInterface
{
    use RefreshTokenTrait, EntityTrait;
}
