<?php

namespace Tominek\OAuth2\Server\Tests\Stubs;

use Tominek\OAuth2\Server\Entities\AuthCodeEntityInterface;
use Tominek\OAuth2\Server\Entities\Traits\AuthCodeTrait;
use Tominek\OAuth2\Server\Entities\Traits\EntityTrait;
use Tominek\OAuth2\Server\Entities\Traits\TokenEntityTrait;

class AuthCodeEntity implements AuthCodeEntityInterface
{
    use EntityTrait, TokenEntityTrait, AuthCodeTrait;
}
