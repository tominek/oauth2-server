<?php

namespace Tominek\OAuth2\Server\Tests\Stubs;

use Tominek\OAuth2\Server\Entities\ClientEntityInterface;
use Tominek\OAuth2\Server\Entities\Traits\ClientTrait;
use Tominek\OAuth2\Server\Entities\Traits\EntityTrait;

class ClientEntity implements ClientEntityInterface
{
    use EntityTrait, ClientTrait;

    public function setRedirectUri($uri)
    {
        $this->redirectUri = $uri;
    }

    public function setName($name)
    {
        $this->name = $name;
    }
}
