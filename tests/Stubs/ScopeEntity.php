<?php

namespace Tominek\OAuth2\Server\Tests\Stubs;

use Tominek\OAuth2\Server\Entities\ScopeEntityInterface;
use Tominek\OAuth2\Server\Entities\Traits\EntityTrait;

class ScopeEntity implements ScopeEntityInterface
{
    use EntityTrait;

    public function jsonSerialize()
    {
        return $this->getIdentifier();
    }
}
