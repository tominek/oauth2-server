<?php

namespace Tominek\OAuth2\Server\Tests\Stubs;

use Symfony\Component\HttpFoundation\Request;
use Tominek\OAuth2\Server\Entities\AccessTokenEntityInterface;
use Tominek\OAuth2\Server\Entities\RefreshTokenEntityInterface;
use Tominek\OAuth2\Server\Exception\OAuthServerException;
use Tominek\OAuth2\Server\ResponseTypes\AbstractResponseType;
use Zend\Diactoros\Response;

class StubResponseType extends AbstractResponseType
{
    public function __construct()
    {
    }

    public function getAccessToken()
    {
        return $this->accessToken;
    }

    public function getRefreshToken()
    {
        return $this->refreshToken;
    }

    /**
     * @param AccessTokenEntityInterface $accessToken
     */
    public function setAccessToken(AccessTokenEntityInterface $accessToken)
    {
        $this->accessToken = $accessToken;
    }

    /**
     * @param RefreshTokenEntityInterface $refreshToken
     */
    public function setRefreshToken(RefreshTokenEntityInterface $refreshToken)
    {
        $this->refreshToken = $refreshToken;
    }

    /**
     * @param Request $request
     *
     * @throws OAuthServerException
     *
     * @return Request
     */
    public function validateAccessToken(Request $request)
    {
        if ($request->get('authorization') === 'Basic test') {
            return $request->attributes->set('oauth_access_token_id', 'test');
        }

        throw OAuthServerException::accessDenied();
    }

    /**
     * @return Response
     */
    public function generateHttpResponse()
    {
        return new Response();
    }
}
