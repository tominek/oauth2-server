<?php

namespace Tominek\OAuth2\Server\Tests\Stubs;

use Tominek\OAuth2\Server\Entities\AccessTokenEntityInterface;
use Tominek\OAuth2\Server\Entities\Traits\AccessTokenTrait;
use Tominek\OAuth2\Server\Entities\Traits\EntityTrait;
use Tominek\OAuth2\Server\Entities\Traits\TokenEntityTrait;

class AccessTokenEntity implements AccessTokenEntityInterface
{
    use AccessTokenTrait, TokenEntityTrait, EntityTrait;
}
