<?php

namespace Tominek\OAuth2\Server\Event;

abstract class AbstractListener implements ListenerInterface
{
    /**
     * {@inheritdoc}
     */
    public function isListener($listener)
    {
        return $this === $listener;
    }
}
