<?php

namespace Tominek\OAuth2\Server\Event;

class Generator implements GeneratorInterface
{
    use GeneratorTrait {
        addEvent as public;
    }
}
