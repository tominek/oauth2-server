<?php
/**
 * OAuth 2.0 Bearer Token Response.
 *
 * @author      Alex Bilbie <hello@alexbilbie.com>
 * @copyright   Copyright (c) Alex Bilbie
 * @license     http://mit-license.org/
 *
 * @link        https://github.com/thephpleague/oauth2-server
 */

namespace Tominek\OAuth2\Server\ResponseTypes;

use Tominek\OAuth2\Server\Entities\AccessTokenEntityInterface;
use Tominek\OAuth2\Server\Entities\RefreshTokenEntityInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

class BearerTokenResponse extends AbstractResponseType
{
    /**
     * {@inheritdoc}
     */
    public function generateHttpResponse()
    {
        $expireDateTime = $this->accessToken->getExpiryDateTime()->getTimestamp();

        $jwtAccessToken = $this->accessToken->convertToJWT($this->privateKey);

        $responseParams = [
            'token_type' => 'Bearer',
            'expires_in' => $expireDateTime - (new \DateTime())->getTimestamp(),
            'access_token' => (string)$jwtAccessToken,
        ];

        if ($this->refreshToken instanceof RefreshTokenEntityInterface) {
            $refreshToken = $this->encrypt(
                json_encode(
                    [
                        'client_id' => $this->accessToken->getClient()->getIdentifier(),
                        'refresh_token_id' => $this->refreshToken->getIdentifier(),
                        'access_token_id' => $this->accessToken->getIdentifier(),
                        'scopes' => $this->accessToken->getScopes(),
                        'user_id' => $this->accessToken->getUserIdentifier(),
                        'expire_time' => $this->refreshToken->getExpiryDateTime()->getTimestamp(),
                    ]
                )
            );

            $responseParams['refresh_token'] = $refreshToken;
        }

        $responseParams = array_merge($this->getExtraParams($this->accessToken), $responseParams);

        return new JsonResponse(
            $responseParams, 200,
            [
                'pragma' => 'no-cache',
                'cache-control' => 'no-store',
                'content-type' => 'application/json',
            ],
            false); //TODO muze byt problem s json_encode
    }

    /**
     * Add custom fields to your Bearer Token response here, then override
     * AuthorizationServer::getResponseType() to pull in your version of
     * this class rather than the default.
     *
     * @param AccessTokenEntityInterface $accessToken
     *
     * @return array
     */
    protected function getExtraParams(AccessTokenEntityInterface $accessToken)
    {
        return [];
    }
}
