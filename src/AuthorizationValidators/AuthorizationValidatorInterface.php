<?php
/**
 * @author      Alex Bilbie <hello@alexbilbie.com>
 * @copyright   Copyright (c) Alex Bilbie
 * @license     http://mit-license.org/
 *
 * @link        https://github.com/thephpleague/oauth2-server
 */

namespace Tominek\OAuth2\Server\AuthorizationValidators;

use Symfony\Component\HttpFoundation\Request;

interface AuthorizationValidatorInterface
{
    /**
     * Determine the access token in the authorization header and append OAUth properties to the request
     *  as attributes.
     *
     * @param Request $request
     *
     * @return Request
     */
    public function validateAuthorization(Request $request);
}
